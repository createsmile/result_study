package com.study.result;

/**
 * 返回的状态码信息
 * @author xupt
 */
public enum ResultCode {
    /* 成功状态码 */
    SUCCESS(1, "操作成功！"),

    /* 错误状态码 */
    FAIL(-1, "操作失败！"),

    /* 参数错误：10001-19999 */
    PARAM_MISSING(10001, "请求参数缺失"),

    /* 用户错误：20001-29999*/
    USER_NOT_LOGGED_IN(20001, "用户未登录，请先登录"),
    USER_NOT_EXIST(20002, "用户不存在"),
    USER_PASSWORD_ERROR(20003, "登录失败，密码错误"),
    /* 业务错误：30001-39999*/
    CASE_NOT_EXIST(30001, "案例不存在"),
    CASE_ITEM_NOT_EXIST(30002,"该案例不存在"),
    CASE_ITEM_FORBIDDEN_EDIT(30003,"该案例不允许修改"),
    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),
    SYSTEM_SEND_EMAIL_ERROR(40002, "邮箱发送错误"),
    SYSTEM_SUBMIT_ERROR(40003, "提交失败"),
    SYSTEM_UPDATE_ERROR(40004, "更新失败"),
    SYSTEM_KEY_DUPLICATE(40005,"字段重复，请重新输入"),
    SYSTEM_INSERT_ERROR(40006, "添加失败"),
    SYSTEM_DELETE_ERROR(40007, "删除失败"),
    SYSTEM_CAPTCHA_ERROR(40008, "验证码错误或已过期"),


    /* 权限错误：70001-79999 */

    PERMISSION_TOKEN_INVALID(70001,"无效token"),
    PERMISSION_FORBIDDEN(70002,"无权利操作");

    /**
     * 操作代码
     */
    int code;
    /**
     * 提示信息
     */
    String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
