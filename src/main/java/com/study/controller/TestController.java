package com.study.controller;

import com.study.common.CustomException;
import com.study.result.ResultCode;
import com.study.result.ResultUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 简单演示
 * @author xupt
 */
@RestController
public class TestController {
    @GetMapping("test1")
    public Object test1() {
        return ResultUtil.success("测试结果成功");
    }

    @GetMapping("test2")
    public Object test2(Integer id) {
        //如果id为0或者null就会全局异常
        if ((10 / id) % 2 == 0) {
            return ResultUtil.success("结果是偶数！！！");
        } else {
            throw new CustomException(ResultCode.PERMISSION_FORBIDDEN);
        }
    }
}
