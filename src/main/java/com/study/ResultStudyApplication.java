package com.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResultStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResultStudyApplication.class, args);
    }

}
